import pprint
import pickle
with open ('mypickle.pickle', 'rb') as default_dict:
    b_dict=pickle.load(default_dict)

print ("=================================")
print ('Your present Dictionary')
pprint.pprint(b_dict)

print('Update your Dictionary with multiple values for a [Key].')
k=input('Enter Key: ')
x=input('Enter multiple values with ONE SPACE between values: ')


v=x.split(' ')
print(" ",v)

d={k:v}
print(d)

b_dict.update(d)

#print(b_dict)
pprint.pprint(b_dict)

with open ('mypickle.pickle' , 'wb') as default_dict:
   pickle.dump(b_dict, default_dict)

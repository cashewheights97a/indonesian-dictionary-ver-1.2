import pickle
import pprint

with open ('mypickle.pickle', 'rb') as default_dict:
    b_dict=pickle.load(default_dict)

print('Practice from your English-Indonesian Vocabulary Dictionary.')
print ("=================================")

#print('Unsorted List - actual order of the dictionary:')
#print(b_dict)

# Print the names of the columns.
print ("=================================")
entries = (len(b_dict.keys()))
n=entries-1
print ('Your present Dictionary is having ',entries , ' entries')
print('You can of course add more words using the update-program')
print()


#print('Sorted List - Not the actual order of the dictionary:')
#pprint.pprint(b_dict)


score=0
i=0


## Text menu in Python
      
def menu():
    print ("=================================")

    print('Choose your option for the program: ')
    print("[1] Start your Indonesian Vocabulary practice here")
    print("[2] Continue to the next word")
    print('[3] Print the sorted dictionary')
    print('[4] Translate an English Word [existing in dictionary] to Indonesian') 
    print("[0] Exit the program. Show your score and where (index) you stopped.")
  
    
  
menu()
option = int(input("Enter your option: "))
 
while option != 0:
    if option == 1:

        A='Enter the index, where you want to start - Choose a value [0-'
        B=']: '
        n=str(n)
        C=(A + n + B)
        k=input(C)
        i=int(k)
        keys_list = list(b_dict)
        key = keys_list[i]
        #print(key)
        value=(b_dict[key])
        #print(value)

        print('English word from dictionary: ',key)# Your starting point
        ans = input('Translate the above English word: ')# Indonesian translation
        if ans in b_dict.get(key):
            print('Good, correct translation!')
            score = score + 5
            i=i+1
        else:
            print('Wrong translation')
            print('The right translation is:')
            print(value)
            print('Choose again: ')
            i+i+1
            
    elif option == 2:
        key = keys_list[i]
        print('English word from dictionary: ',key)# Your starting point
        ans = input('Translate the English word: ')# Indonesian translation
        if ans in b_dict.get(key):
            print('Good, correct translation!')
            print()
            score = score + 5
            i=i+1
        else:
            print('Wrong translation')
            print('The right translation is:')
            print(b_dict.get(key))
            print()
            print('Choose again: ')
            i+i+1
            

    elif option == 3:
        pprint.pprint(b_dict)
        

    elif option == 4:
         key1=input('Enter the English word you want translated: ')
         try:
          value1=(b_dict[key1])
          print(value1)
         except:
          print('The word not in the dictionary')
          print()
          pass
    
    else:
        print("Invalid option. Choose again!")
     
    menu()                                
    option = int(input("Enter your option: "))
    
print('Your Score is: ', score)

if i==0:
   print('You have not started the index. So, start at index 0 next time! Goodbye!')
else:
   print('You stopped at index ',i,' Start at ',i+1,' next time.')
   print("Goodbye!")


   

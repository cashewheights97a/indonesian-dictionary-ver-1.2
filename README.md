# ENGLISH to INDONESIAN Dictionary  For Learning#
## <p style="text-align: center;">in Python3</p>
## <p style="text-align: center;">Version 1.2</p>
You may download, use, modify and distribute as you please.

This program is written in Python 3. The program will be useful for those interested in learning how to develop a Python Dictionary. The dictionary is in English -->> Indonesian format. The program can be used by those who are learning Indonesian at beginners level.

Presently, there are six files:

1. Readme - which is this file giving you the explanations.
2. mypickle.pikle - folder containing the binary dictionary file (only machine readable). The present version is populated with 159 entries. This dictionary can only be accessed with the three python programs. You can of course add more words using the update-program
3. menu_program_01.py - main python program to access the dictionary file. The following options are availble:

	 +Start your Indonesian Vocabulary practice here

	 +Continue to the next word

	+Print the sorted dictionary

	+Translate an English Word [existing in dictionary] to Indonesian
	
	+Exit the program. Show your score and where (index) you stopped
  

4. update_default_ind_dict.py - this program can be used to add new words to the dictionary file.
5. Multiple-Value-Dictionary-Input.py - this can be used populate entries with multiple values
6. deleteing-from-dictionary.py- this can be used to delete an entry

All files should be put in one directory.
You need Python 3 program to run the files.
If you have any question, please write to me at kalyan.chatterjea@yahoo.com.sg
Thanks for using these programs.

Kalyan Chatterjea

Singapore

Wednesday, 09. March 2022 07:26PM 

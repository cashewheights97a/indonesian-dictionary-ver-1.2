import pickle
import pprint
with open ('mypickle.pickle', 'rb') as default_dict:
    b_dict=pickle.load(default_dict)

print ('Your present Dictionary is having ', (len(b_dict.keys())), ' words')
pp = pprint.PrettyPrinter(indent=4)

pp.pprint(b_dict)

print('Update your Dictionary by deleting wrong key-value pair.')
print ("=================================")


remove_key=input('Enter key to remove: ')

#if remove_key in b_dict:
#    del b_dict[remove_key]


b_dict.pop(remove_key)

pp.pprint(b_dict)

with open ('mypickle.pickle' , 'wb') as default_dict:
   pickle.dump(b_dict, default_dict)

